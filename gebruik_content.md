# Gebruik Content

Markdown is in principe de GitLab Flavour (Kramdown) inclusief Kroki.io diagram Api rendering.

*PlantUML voorbeeld:*

```plantuml
@startuml@startuml
start
:Lees **SUMMARY** Header als titel en opsomregels als **Markdown** links;
:Parsen van **Markdown** links als **Sidebar** menu-items;
:Voorbereiden **Markdown** gelinkte content (zoals *Kroki.io* diagram Api links);
:Schrijf **VuePress publicatie** met voorbereide content;
stop
@enduml
```

*Asset plaatje voorbeeld (vanuit link naar plaatje geincludeerd):*

![Avatar](./assets/OnnoAvatar.svg)
