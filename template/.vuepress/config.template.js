/**
 * VuePress-config parsed by MdBook CLI: https://gitlab.com/istddev/pub-tools/mdbook
 */
module.exports = {
    base: '<%= base %>',
    dest: '<%= dest %>',
    description: '<%= description %>',
    theme: 'book',
    themeConfig: {
        logo: '<%= logo %>',
        nav: [
            {
                text: 'PDF export',
                link: '<%= pdfExport %>'
            },
            {
                text: 'ePub export',
                link: '<%= epubExport %>'
            },
            {
                text: 'GitLab repo',
                link: '<%= gitRepo %>'
            },
        ],
        sidebar: {
            '/': [<%= sidebar %>
            ] 
        },
    },
    markdown: {
        extendMarkdown: md => {
            md.set({ breaks: true })
            md.use(require('markdown-it-footnote'))
        }
    },
    plugins: [
        ['vuepress-plugin-code-copy', true],
        [
            'vuepress-plugin-clean-urls',
            {
                normalSuffix: '/'
            }
        ],
        ['fulltext-search'],
        ['vuepress-plugin-glossary']
    ],
};
