# Wijzigingen

## [V0.9.0-rc1]

### Eerste Relaesekandidaat ter review PPIM-SA collega's

- ![1](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8167154/avatar.png?width=40) [Onno Haldar](https://gitlab.com/onnohaldar)

  - CLI commando's compleet gemaakt voor ontwikkelen en publiceren

  - Documentatie compleet gemaakt en geheel in Nederlands 


## [V0.8.0-verkenning]

### Nieuwe inzichten verwerken voor conversie google docs KIK-V Explianer naar Boek-publicatie

- ![1](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8167154/avatar.png?width=40) [Onno Haldar](https://gitlab.com/onnohaldar)
  - Opdelen van 1 Markdown-document op basis van hoofdsecties (Heading Level 1)

## [V0.7.5-verkenning] 24-03-2010

### Initieel

- ![1](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8167154/avatar.png?width=40) [Onno Haldar](https://gitlab.com/onnohaldar)
  - Initieel werkend met SUMMARY.md opzet

