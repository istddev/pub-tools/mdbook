/**
 * GitLab Configuration
 */
export declare class GitLabConfig {
    private runningEnv;
    /**
     * Constructs GitLab Configuration values Class
     * @param env Environment variables of current process (local or GitLab CI)
     */
    constructor(runningEnv: any);
    /**
     * User or Group Page doesn't have a sub path but ends with pages domain name
     * See https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names
     */
    get isUserOrGroupPage(): boolean;
    /**
     * See CI variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ci(): boolean;
    /**
     * See CI_PAGES_DOMAIN variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciPagesDomain(): string;
    /**
     * See CI_PAGES_URL variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciPagesUrl(): string;
    /**
     * See CI_PROJECT_ROOT_NAMESPACE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectRootNameSpace(): string;
    /**
     * See CI_PROJECT_NAMESPACE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectNameSpace(): string;
    /**
     * See CI_PROJECT_NAME variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectName(): string;
    /**
     * See CI_PROJECT_PATH variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectPath(): string;
    /**
     * See CI_PROJECT_TITLE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectTitle(): string;
    /**
     * See CI_PROJECT_URL variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectUrl(): string;
}
