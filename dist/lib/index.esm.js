import { existsSync, readFileSync, writeFileSync } from 'fs';
import { join, resolve, sep } from 'path';
import { copySync } from 'cpx';
import { resolveTemplatePath, getSummarySections, getMdHeaderStr, createTemplate, getMdContentLinkRef, getMdContentLinkName } from '@pub-tools/mdlib-ts';
import { execSync } from 'child_process';
import { writePreProcessedDestFile, preProcessKrokiMdContent } from '@dgwnu/md-pre-kroki';

/**
 * GitLab Configuration
 */
var GitLabConfig = /** @class */ (function () {
    /**
     * Constructs GitLab Configuration values Class
     * @param env Environment variables of current process (local or GitLab CI)
     */
    function GitLabConfig(runningEnv) {
        this.runningEnv = runningEnv;
        console.log("runningEnv.CI = " + runningEnv.CI);
    }
    Object.defineProperty(GitLabConfig.prototype, "isUserOrGroupPage", {
        /**
         * User or Group Page doesn't have a sub path but ends with pages domain name
         * See https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names
         */
        get: function () {
            return this.ciPagesUrl.endsWith(this.ciPagesDomain);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ci", {
        /**
         * See CI variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return Boolean(this.runningEnv.CI);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciPagesDomain", {
        /**
         * See CI_PAGES_DOMAIN variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PAGES_DOMAIN);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciPagesUrl", {
        /**
         * See CI_PAGES_URL variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PAGES_URL);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciProjectRootNameSpace", {
        /**
         * See CI_PROJECT_ROOT_NAMESPACE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PROJECT_ROOT_NAMESPACE);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciProjectNameSpace", {
        /**
         * See CI_PROJECT_NAMESPACE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PROJECT_NAMESPACE);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciProjectName", {
        /**
         * See CI_PROJECT_NAME variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PROJECT_NAME);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciProjectPath", {
        /**
         * See CI_PROJECT_PATH variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PROJECT_PATH);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciProjectTitle", {
        /**
         * See CI_PROJECT_TITLE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PROJECT_TITLE);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(GitLabConfig.prototype, "ciProjectUrl", {
        /**
         * See CI_PROJECT_URL variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
         */
        get: function () {
            return String(this.runningEnv.CI_PROJECT_URL);
        },
        enumerable: false,
        configurable: true
    });
    return GitLabConfig;
}());

/**
 * Mardown Book Publication Configuration
 */
var MdBookConfig = /** @class */ (function () {
    /**
     * Constructs Markdown Book Configuration Class
     * @param gitLab GitLab CI Running environment (in case of GitLab CI runner execution)
     * @param cliParms Command Line parameters
     */
    function MdBookConfig(gitLab, cliParms) {
        this.gitLab = gitLab;
        this.cliParms = cliParms;
        this.vuePressConfig = '.vuepress';
        this.vuePressPublic = join(this.vuePressConfig, 'public');
        this.vuePressStyles = join(this.vuePressConfig, 'styles');
        this.vuePressConfigTemplateJs = join(this.vuePressConfig, 'config.template.js');
        this.vuePressDependenciesJson = join(this.vuePressConfig, 'vuepress-dependencies.json');
        this.vuePressConfigJs = join(this.vuePressConfig, 'config.js');
        this.siteMdFile = 'site.md';
        this.customLogoFile = 'logo.png';
        this.publication = {
            base: '/',
            dest: 'public',
            logo: '/book.png',
            pdfExport: 'https://istddev.gitlab.io/pub-tools/mdbook/mdbook.pdf',
            epubExport: 'https://istddev.gitlab.io/pub-tools/mdbook/mdbook.epub',
            gitRepo: 'https://gitlab.com/istddev/pub-tools/mdbook'
        };
        /**
         * Site Export Language Metadata
         */
        this.lang = 'nl-NL';
        if (this.gitLab.ci) {
            this.initGitLabCi();
        }
        this.setCustomLogo();
    }
    /**
     * Initialize Running GitLab CI Configuration values
     */
    MdBookConfig.prototype.initGitLabCi = function () {
        if (!this.gitLab.isUserOrGroupPage) {
            // Set Markdown Book publication base path
            var repoPaths = this.gitLab.ciProjectPath.split('/');
            var repoGroup = repoPaths.length > 1 ? '/' + repoPaths[repoPaths.length - 2] : '';
            var repoProject = '/' + repoPaths[repoPaths.length - 1] + '/';
            this.publication.base = repoGroup + repoProject;
        }
        this.publication.pdfExport = this.gitLab.ciPagesUrl + '/' + this.gitLab.ciProjectName + '.pdf';
        this.publication.epubExport = this.gitLab.ciPagesUrl + '/' + this.gitLab.ciProjectName + '.epub';
        this.publication.gitRepo = this.gitLab.ciProjectUrl;
    };
    /**
     * Set custom publication logo ("logo.png" in repo Markdown content directory)
     */
    MdBookConfig.prototype.setCustomLogo = function () {
        // custom logo should be available in Markdown content directory
        var customLogoFilePath = join(this.mdContentPath, this.customLogoFile);
        if (existsSync(customLogoFilePath)) {
            // copy en set custom defined logo
            copySync(customLogoFilePath, this.docsPublicPath);
            this.publication.logo = '/' + this.customLogoFile;
        }
    };
    Object.defineProperty(MdBookConfig.prototype, "docsPath", {
        /**
         * Resolved Output directory path to parse VuePress formated data
         */
        get: function () {
            return resolve(this.cliParms.docsPath);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "docsPublicPath", {
        /**
         * Resolved VuePress Public output directory path (output-files directly accessible in site publication)
         */
        get: function () {
            return join(this.docsPath, this.vuePressPublic);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "docsStylesPath", {
        /**
         * Resolved VuePress Styles output directory path
         */
        get: function () {
            return join(this.docsPath, this.vuePressStyles);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "mdContentPath", {
        /**
         * Resolved Input directory path that should contain Md Book formated Markdown data
         */
        get: function () {
            return resolve(this.cliParms.mdContentPath);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "docsConfigJsFilePath", {
        /**
         * Resolved Path to VuePress Config Output JS File
         */
        get: function () {
            return join(this.docsPath, this.vuePressConfigJs);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "templatePath", {
        /**
         * Resolved VuePress Template directory path (MdBook-package or customomized-repo)
         */
        get: function () {
            return resolveTemplatePath(__dirname, this.cliParms.templatePath);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "templatePublicPath", {
        /**
         * Resolved Public template directory path (template-files directly accessible in site publication)
         */
        get: function () {
            return join(this.templatePath, this.vuePressPublic);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "templateStylesPath", {
        /**
         * Resolved Path to VuePress Styles template directory
         */
        get: function () {
            return join(this.templatePath, this.vuePressStyles);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "templateConfigJsFilePath", {
        /**
         * Resolved Path to VuePress Config Template JS File
         */
        get: function () {
            return join(this.templatePath, this.vuePressConfigTemplateJs);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "vuePressDependenciesJsonFilePath", {
        /**
         * Resolved Path to VuePress Depedencies JSON File
         */
        get: function () {
            return join(this.templatePath, this.vuePressDependenciesJson);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "siteMdFilePath", {
        /**
         * Resolved Path to VuePress Site Export Input File (to create PDF, ePub, ...etc. exports)
         */
        get: function () {
            return join(this.docsPath, this.siteMdFile);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "publicationBase", {
        /**
         * GitLab Pages Publication base url path
         */
        get: function () {
            return this.publication.base;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "publicationDest", {
        /**
         * GitLab Pages Publication destination directory path
         */
        get: function () {
            return this.publication.dest;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "publicationLogo", {
        /**
         * GitLab Pages Publication logo path
         */
        get: function () {
            return this.publication.logo;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "publicationPdfExport", {
        /**
         * GitLab Pages Publication PDF export URL
         */
        get: function () {
            return this.publication.pdfExport;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "publicationEpubExport", {
        /**
         * GitLab Pages Publication ePub export URL
         */
        get: function () {
            return this.publication.epubExport;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(MdBookConfig.prototype, "publicationGitRepo", {
        /**
         * GitLab Pages Publication GIT repository URL
         */
        get: function () {
            return this.publication.gitRepo;
        },
        enumerable: false,
        configurable: true
    });
    return MdBookConfig;
}());

// Markdown Book Publication Utils
// Constant Values
var SUMMARY_MD = 'SUMMARY.md';
var ROOT_LINKS = ['README', 'index'];
var PANDOC_CONFIG = 'pandoc';
var PANDOC_PDF_TEMPLATE = join(PANDOC_CONFIG, 'pandoc-pdf.template');
/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param config Markdown Book Configuration values
 */
function parseMd2Book(config) {
    console.log('='.repeat(40));
    console.log('MdBookConfig values (TypeScript)');
    console.log('-'.repeat(40));
    console.log("templatePath = " + config.templatePath);
    console.log("templatePublicPath = " + config.templatePublicPath);
    console.log("templateConfigJsFilePath = " + config.templateConfigJsFilePath);
    console.log("vuePressDependenciesJsonFilePath = " + config.vuePressDependenciesJsonFilePath);
    console.log("mdContentPath = " + config.mdContentPath);
    console.log("docsPath = " + config.docsPath);
    console.log("docsPublicPath = " + config.docsPublicPath);
    console.log("docsConfigJsFilePath = " + config.docsConfigJsFilePath);
    console.log("publicationBase = " + config.publicationBase);
    console.log("publicationDest = " + config.publicationDest);
    console.log("publicationLogo = " + config.publicationLogo);
    console.log("publicationPdfExport = " + config.publicationPdfExport);
    console.log("publicationEpubExport = " + config.publicationEpubExport);
    console.log("publicationGitRepo = " + config.publicationGitRepo);
    console.log('='.repeat(40));
    // check existance required SUMMARY
    var summaryFilePath = join(config.mdContentPath, SUMMARY_MD);
    if (!existsSync(summaryFilePath)) {
        throw new Error("MdBook CLI - Missing required \"" + SUMMARY_MD + "\" file!");
    }
    // Initialize vuePress docs output with template and defaults
    copySync("" + config.templatePublicPath + sep + "**", config.docsPublicPath);
    copySync("" + config.templateStylesPath + sep + "**", config.docsStylesPath);
    copySync(join(config.templatePath, PANDOC_PDF_TEMPLATE), join(config.docsPath, PANDOC_PDF_TEMPLATE));
    // Read SUMMARY to process content
    var summaryMdStr = readFileSync(summaryFilePath, 'utf-8');
    var summarySections = getSummarySections(summaryMdStr);
    // Parse VuePress-configuration values into template
    var description = getMdHeaderStr(summaryMdStr);
    var sidebar = preProcessParseSidebar(summarySections, config);
    var configTemplateJs = readFileSync(config.templateConfigJsFilePath, 'utf-8');
    writeFileSync(config.docsConfigJsFilePath, configTemplateJs
        .replace(createTemplate('base'), config.publicationBase)
        .replace(createTemplate('dest'), config.publicationDest)
        .replace(createTemplate('description'), description)
        .replace(createTemplate('logo'), config.publicationLogo)
        .replace(createTemplate('sidebar'), sidebar)
        .replace(createTemplate('pdfExport'), config.publicationPdfExport)
        .replace(createTemplate('epubExport'), config.publicationEpubExport)
        .replace(createTemplate('gitRepo'), config.publicationGitRepo));
    // export consolidated site markdown-file from pre-processed content
    exportSiteMd(summarySections, config);
}
/**
 * Use summary sections data to:
 * - Pre-process markdown content referenced in summary section lines
 * - Parse sidebar string that can be used in the VuePress config.js template
 *
 * @param summarySections summary sections data
 * @param config Md Book Configuration Values
 *
 * @returns sidebar string to parse into VuePress config.js template
 */
function preProcessParseSidebar(summarySections, config) {
    // parse sidebar config
    var sidebar = '';
    for (var _i = 0, summarySections_1 = summarySections; _i < summarySections_1.length; _i++) {
        var section = summarySections_1[_i];
        var sectionChildren = '';
        var _loop_1 = function (line) {
            // read Markdown Content from input directory
            var mdFileLink = getMdContentLinkRef(line);
            var mdContentFilePath = join(config.mdContentPath, mdFileLink);
            var mdContentStr = readFileSync(mdContentFilePath, 'utf-8');
            // write Markdown Content to output directory (including pre processing Kroki.io Apis call's)
            writePreProcessedDestFile(config.mdContentPath, config.docsPath, mdContentFilePath, preProcessKrokiMdContent(mdContentStr));
            // prepare section-children for parsing into sidebar-config
            var childLink = '/' + mdFileLink.split('.')[0];
            var rootLink = ROOT_LINKS.find(function (link) { return childLink.endsWith(link); });
            if (rootLink) {
                // a rootlink must point to the directory (remove rootlink name)
                childLink = childLink.substr(0, childLink.length - rootLink.length);
            }
            var childName = getMdContentLinkName(line);
            sectionChildren += "                    ['" + childLink + "', '" + childName + "'],\n";
        };
        for (var _a = 0, _b = section.summaryLines; _a < _b.length; _a++) {
            var line = _b[_a];
            _loop_1(line);
        }
        sidebar += "\n               {\n                  title: '" + section.title + "',\n                  collapsable: false,\n                  sidebarDepth: 0,\n                  children: [\n" + sectionChildren + "                  ]\n               },";
    }
    return sidebar;
}
/**
 * Export site markdown files as one Markdown-file based on summary sections
 * @param summarySections summary sections with lines that contains references to the site markdown files
 * @param config Markdown Book Configuration Values
 */
function exportSiteMd(summarySections, config) {
    // initialize markdown vars
    var siteMdTitle = '';
    var siteMdContent = '';
    // Build markdown file from docs content
    for (var _i = 0, summarySections_2 = summarySections; _i < summarySections_2.length; _i++) {
        var section = summarySections_2[_i];
        siteMdContent += '## ' + section.title + '\n\n';
        for (var _a = 0, _b = section.summaryLines; _a < _b.length; _a++) {
            var line = _b[_a];
            // add section content
            var mdFileLink = getMdContentLinkRef(line);
            var mdDocsFilePath = join(config.docsPath, mdFileLink);
            var mdLines = readFileSync(mdDocsFilePath, 'utf-8').split('\n');
            var mdContent = '';
            // increment markdown header levels
            for (var _c = 0, mdLines_1 = mdLines; _c < mdLines_1.length; _c++) {
                var mdLine = mdLines_1[_c];
                if (mdLine.startsWith('#')) {
                    // Header Line
                    if (siteMdTitle == '') {
                        // first H1 is site title
                        // create YAML metadata for PANDOC https://pandoc.org/MANUAL.html#epub-metadata
                        siteMdTitle = [
                            '---',
                            "title: " + mdLine.split(' ')[1],
                            "lang: " + config.lang,
                            '---',
                            '\n'
                        ].join('\n');
                    }
                    else {
                        // Add Header as content line
                        mdContent += mdLine + '\n';
                    }
                }
                else {
                    // Add Content line
                    mdContent += mdLine + '\n';
                }
            }
            siteMdContent += mdContent;
        }
    }
    writeFileSync(config.siteMdFilePath, "" + siteMdTitle + siteMdContent);
}
/**
 * Install VuePress Template Dependencies
 * @param docsPath path to parsed VuePress docs build (required)
 */
function InstallVuePressTemplateDependencies(config) {
    // install VuePress dependencies (VuePress, Theme, MarkedIt, Plugins, etc..)
    var dependencies = JSON.parse(readFileSync(config.vuePressDependenciesJsonFilePath, 'utf-8'));
    var dependenciesInstallResult = execSync("npm install " + dependencies.join(' ') + " --no-save").toString();
    console.log("dependenciesInstallResult = " + dependenciesInstallResult);
}

export { GitLabConfig, InstallVuePressTemplateDependencies, MdBookConfig, parseMd2Book };
