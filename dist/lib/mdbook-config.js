"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MdBookConfig = void 0;
const fs_1 = require("fs");
const path_1 = require("path");
const cpx_1 = require("cpx");
const mdlib_ts_1 = require("@pub-tools/mdlib-ts");
/**
 * Mardown Book Publication Configuration
 */
class MdBookConfig {
    /**
     * Constructs Markdown Book Configuration Class
     * @param gitLab GitLab CI Running environment (in case of GitLab CI runner execution)
     * @param cliParms Command Line parameters
     */
    constructor(gitLab, cliParms) {
        this.gitLab = gitLab;
        this.cliParms = cliParms;
        this.vuePressConfig = '.vuepress';
        this.vuePressPublic = path_1.join(this.vuePressConfig, 'public');
        this.vuePressStyles = path_1.join(this.vuePressConfig, 'styles');
        this.vuePressConfigTemplateJs = path_1.join(this.vuePressConfig, 'config.template.js');
        this.vuePressDependenciesJson = path_1.join(this.vuePressConfig, 'vuepress-dependencies.json');
        this.vuePressConfigJs = path_1.join(this.vuePressConfig, 'config.js');
        this.siteMdFile = 'site.md';
        this.customLogoFile = 'logo.png';
        this.publication = {
            base: '/',
            dest: 'public',
            logo: '/book.png',
            pdfExport: 'https://istddev.gitlab.io/pub-tools/mdbook/mdbook.pdf',
            epubExport: 'https://istddev.gitlab.io/pub-tools/mdbook/mdbook.epub',
            gitRepo: 'https://gitlab.com/istddev/pub-tools/mdbook'
        };
        /**
         * Site Export Language Metadata
         */
        this.lang = 'nl-NL';
        if (this.gitLab.ci) {
            this.initGitLabCi();
        }
        this.setCustomLogo();
    }
    /**
     * Initialize Running GitLab CI Configuration values
     */
    initGitLabCi() {
        if (!this.gitLab.isUserOrGroupPage) {
            // Set Markdown Book publication base path
            const repoPaths = this.gitLab.ciProjectPath.split('/');
            const repoGroup = repoPaths.length > 1 ? '/' + repoPaths[repoPaths.length - 2] : '';
            const repoProject = '/' + repoPaths[repoPaths.length - 1] + '/';
            this.publication.base = repoGroup + repoProject;
        }
        this.publication.pdfExport = this.gitLab.ciPagesUrl + '/' + this.gitLab.ciProjectName + '.pdf';
        this.publication.epubExport = this.gitLab.ciPagesUrl + '/' + this.gitLab.ciProjectName + '.epub';
        this.publication.gitRepo = this.gitLab.ciProjectUrl;
    }
    /**
     * Set custom publication logo ("logo.png" in repo Markdown content directory)
     */
    setCustomLogo() {
        // custom logo should be available in Markdown content directory
        const customLogoFilePath = path_1.join(this.mdContentPath, this.customLogoFile);
        if (fs_1.existsSync(customLogoFilePath)) {
            // copy en set custom defined logo
            cpx_1.copySync(customLogoFilePath, this.docsPublicPath);
            this.publication.logo = '/' + this.customLogoFile;
        }
    }
    /**
     * Resolved Output directory path to parse VuePress formated data
     */
    get docsPath() {
        return path_1.resolve(this.cliParms.docsPath);
    }
    /**
     * Resolved VuePress Public output directory path (output-files directly accessible in site publication)
     */
    get docsPublicPath() {
        return path_1.join(this.docsPath, this.vuePressPublic);
    }
    /**
     * Resolved VuePress Styles output directory path
     */
    get docsStylesPath() {
        return path_1.join(this.docsPath, this.vuePressStyles);
    }
    /**
     * Resolved Input directory path that should contain Md Book formated Markdown data
     */
    get mdContentPath() {
        return path_1.resolve(this.cliParms.mdContentPath);
    }
    /**
     * Resolved Path to VuePress Config Output JS File
     */
    get docsConfigJsFilePath() {
        return path_1.join(this.docsPath, this.vuePressConfigJs);
    }
    /**
     * Resolved VuePress Template directory path (MdBook-package or customomized-repo)
     */
    get templatePath() {
        return mdlib_ts_1.resolveTemplatePath(__dirname, this.cliParms.templatePath);
    }
    /**
     * Resolved Public template directory path (template-files directly accessible in site publication)
     */
    get templatePublicPath() {
        return path_1.join(this.templatePath, this.vuePressPublic);
    }
    /**
     * Resolved Path to VuePress Styles template directory
     */
    get templateStylesPath() {
        return path_1.join(this.templatePath, this.vuePressStyles);
    }
    /**
     * Resolved Path to VuePress Config Template JS File
     */
    get templateConfigJsFilePath() {
        return path_1.join(this.templatePath, this.vuePressConfigTemplateJs);
    }
    /**
     * Resolved Path to VuePress Depedencies JSON File
     */
    get vuePressDependenciesJsonFilePath() {
        return path_1.join(this.templatePath, this.vuePressDependenciesJson);
    }
    /**
     * Resolved Path to VuePress Site Export Input File (to create PDF, ePub, ...etc. exports)
     */
    get siteMdFilePath() {
        return path_1.join(this.docsPath, this.siteMdFile);
    }
    /**
     * GitLab Pages Publication base url path
     */
    get publicationBase() {
        return this.publication.base;
    }
    /**
     * GitLab Pages Publication destination directory path
     */
    get publicationDest() {
        return this.publication.dest;
    }
    /**
     * GitLab Pages Publication logo path
     */
    get publicationLogo() {
        return this.publication.logo;
    }
    /**
     * GitLab Pages Publication PDF export URL
     */
    get publicationPdfExport() {
        return this.publication.pdfExport;
    }
    /**
     * GitLab Pages Publication ePub export URL
     */
    get publicationEpubExport() {
        return this.publication.epubExport;
    }
    /**
     * GitLab Pages Publication GIT repository URL
     */
    get publicationGitRepo() {
        return this.publication.gitRepo;
    }
}
exports.MdBookConfig = MdBookConfig;
