"use strict";
// Markdown Book Publication Utils
Object.defineProperty(exports, "__esModule", { value: true });
exports.InstallVuePressTemplateDependencies = exports.parseMd2Book = void 0;
// Node Package Imports
const child_process_1 = require("child_process");
const fs_1 = require("fs");
const path_1 = require("path");
const cpx_1 = require("cpx");
const mdlib_ts_1 = require("@pub-tools/mdlib-ts");
// Open Source van eigen maak (n.t.b. onderbrengen waar?)
const md_pre_kroki_1 = require("@dgwnu/md-pre-kroki");
// Constant Values
const SUMMARY_MD = 'SUMMARY.md';
const ROOT_LINKS = ['README', 'index'];
const PANDOC_CONFIG = 'pandoc';
const PANDOC_PDF_TEMPLATE = path_1.join(PANDOC_CONFIG, 'pandoc-pdf.template');
/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param config Markdown Book Configuration values
 */
function parseMd2Book(config) {
    console.log('='.repeat(40));
    console.log('MdBookConfig values (TypeScript)');
    console.log('-'.repeat(40));
    console.log(`templatePath = ${config.templatePath}`);
    console.log(`templatePublicPath = ${config.templatePublicPath}`);
    console.log(`templateConfigJsFilePath = ${config.templateConfigJsFilePath}`);
    console.log(`vuePressDependenciesJsonFilePath = ${config.vuePressDependenciesJsonFilePath}`);
    console.log(`mdContentPath = ${config.mdContentPath}`);
    console.log(`docsPath = ${config.docsPath}`);
    console.log(`docsPublicPath = ${config.docsPublicPath}`);
    console.log(`docsConfigJsFilePath = ${config.docsConfigJsFilePath}`);
    console.log(`publicationBase = ${config.publicationBase}`);
    console.log(`publicationDest = ${config.publicationDest}`);
    console.log(`publicationLogo = ${config.publicationLogo}`);
    console.log(`publicationPdfExport = ${config.publicationPdfExport}`);
    console.log(`publicationEpubExport = ${config.publicationEpubExport}`);
    console.log(`publicationGitRepo = ${config.publicationGitRepo}`);
    console.log('='.repeat(40));
    // check existance required SUMMARY
    const summaryFilePath = path_1.join(config.mdContentPath, SUMMARY_MD);
    if (!fs_1.existsSync(summaryFilePath)) {
        throw new Error(`MdBook CLI - Missing required "${SUMMARY_MD}" file!`);
    }
    // Initialize vuePress docs output with template and defaults
    cpx_1.copySync(`${config.templatePublicPath}${path_1.sep}**`, config.docsPublicPath);
    cpx_1.copySync(`${config.templateStylesPath}${path_1.sep}**`, config.docsStylesPath);
    cpx_1.copySync(path_1.join(config.templatePath, PANDOC_PDF_TEMPLATE), path_1.join(config.docsPath, PANDOC_PDF_TEMPLATE));
    // Read SUMMARY to process content
    const summaryMdStr = fs_1.readFileSync(summaryFilePath, 'utf-8');
    const summarySections = mdlib_ts_1.getSummarySections(summaryMdStr);
    // Parse VuePress-configuration values into template
    const description = mdlib_ts_1.getMdHeaderStr(summaryMdStr);
    const sidebar = preProcessParseSidebar(summarySections, config);
    const configTemplateJs = fs_1.readFileSync(config.templateConfigJsFilePath, 'utf-8');
    fs_1.writeFileSync(config.docsConfigJsFilePath, configTemplateJs
        .replace(mdlib_ts_1.createTemplate('base'), config.publicationBase)
        .replace(mdlib_ts_1.createTemplate('dest'), config.publicationDest)
        .replace(mdlib_ts_1.createTemplate('description'), description)
        .replace(mdlib_ts_1.createTemplate('logo'), config.publicationLogo)
        .replace(mdlib_ts_1.createTemplate('sidebar'), sidebar)
        .replace(mdlib_ts_1.createTemplate('pdfExport'), config.publicationPdfExport)
        .replace(mdlib_ts_1.createTemplate('epubExport'), config.publicationEpubExport)
        .replace(mdlib_ts_1.createTemplate('gitRepo'), config.publicationGitRepo));
    // export consolidated site markdown-file from pre-processed content
    exportSiteMd(summarySections, config);
}
exports.parseMd2Book = parseMd2Book;
/**
 * Use summary sections data to:
 * - Pre-process markdown content referenced in summary section lines
 * - Parse sidebar string that can be used in the VuePress config.js template
 *
 * @param summarySections summary sections data
 * @param config Md Book Configuration Values
 *
 * @returns sidebar string to parse into VuePress config.js template
 */
function preProcessParseSidebar(summarySections, config) {
    // parse sidebar config
    let sidebar = '';
    for (const section of summarySections) {
        let sectionChildren = '';
        for (const line of section.summaryLines) {
            // read Markdown Content from input directory
            const mdFileLink = mdlib_ts_1.getMdContentLinkRef(line);
            const mdContentFilePath = path_1.join(config.mdContentPath, mdFileLink);
            let mdContentStr = fs_1.readFileSync(mdContentFilePath, 'utf-8');
            // write Markdown Content to output directory (including pre processing Kroki.io Apis call's)
            md_pre_kroki_1.writePreProcessedDestFile(config.mdContentPath, config.docsPath, mdContentFilePath, md_pre_kroki_1.preProcessKrokiMdContent(mdContentStr));
            // prepare section-children for parsing into sidebar-config
            let childLink = '/' + mdFileLink.split('.')[0];
            const rootLink = ROOT_LINKS.find(link => childLink.endsWith(link));
            if (rootLink) {
                // a rootlink must point to the directory (remove rootlink name)
                childLink = childLink.substr(0, childLink.length - rootLink.length);
            }
            const childName = mdlib_ts_1.getMdContentLinkName(line);
            sectionChildren += `                    ['${childLink}', '${childName}'],
`;
        }
        sidebar += `
               {
                  title: '${section.title}',
                  collapsable: false,
                  sidebarDepth: 0,
                  children: [
${sectionChildren}                  ]
               },`;
    }
    return sidebar;
}
/**
 * Export site markdown files as one Markdown-file based on summary sections
 * @param summarySections summary sections with lines that contains references to the site markdown files
 * @param config Markdown Book Configuration Values
 */
function exportSiteMd(summarySections, config) {
    // initialize markdown vars
    let siteMdTitle = '';
    let siteMdContent = '';
    // Build markdown file from docs content
    for (const section of summarySections) {
        siteMdContent += '## ' + section.title + '\n\n';
        for (const line of section.summaryLines) {
            // add section content
            const mdFileLink = mdlib_ts_1.getMdContentLinkRef(line);
            const mdDocsFilePath = path_1.join(config.docsPath, mdFileLink);
            const mdLines = fs_1.readFileSync(mdDocsFilePath, 'utf-8').split('\n');
            let mdContent = '';
            // increment markdown header levels
            for (const mdLine of mdLines) {
                if (mdLine.startsWith('#')) {
                    // Header Line
                    if (siteMdTitle == '') {
                        // first H1 is site title
                        // create YAML metadata for PANDOC https://pandoc.org/MANUAL.html#epub-metadata
                        siteMdTitle = [
                            '---',
                            `title: ${mdLine.split(' ')[1]}`,
                            `lang: ${config.lang}`,
                            '---',
                            '\n'
                        ].join('\n');
                    }
                    else {
                        // Add Header as content line
                        mdContent += mdLine + '\n';
                    }
                }
                else {
                    // Add Content line
                    mdContent += mdLine + '\n';
                }
            }
            siteMdContent += mdContent;
        }
    }
    fs_1.writeFileSync(config.siteMdFilePath, `${siteMdTitle}${siteMdContent}`);
}
/**
 * Install VuePress Template Dependencies
 * @param docsPath path to parsed VuePress docs build (required)
 */
function InstallVuePressTemplateDependencies(config) {
    // install VuePress dependencies (VuePress, Theme, MarkedIt, Plugins, etc..)
    const dependencies = JSON.parse(fs_1.readFileSync(config.vuePressDependenciesJsonFilePath, 'utf-8'));
    const dependenciesInstallResult = child_process_1.execSync(`npm install ${dependencies.join(' ')} --no-save`).toString();
    console.log(`dependenciesInstallResult = ${dependenciesInstallResult}`);
}
exports.InstallVuePressTemplateDependencies = InstallVuePressTemplateDependencies;
