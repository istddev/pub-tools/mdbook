"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GitLabConfig = void 0;
/**
 * GitLab Configuration
 */
class GitLabConfig {
    /**
     * Constructs GitLab Configuration values Class
     * @param env Environment variables of current process (local or GitLab CI)
     */
    constructor(runningEnv) {
        this.runningEnv = runningEnv;
        console.log(`runningEnv.CI = ${runningEnv.CI}`);
    }
    /**
     * User or Group Page doesn't have a sub path but ends with pages domain name
     * See https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names
     */
    get isUserOrGroupPage() {
        return this.ciPagesUrl.endsWith(this.ciPagesDomain);
    }
    /**
     * See CI variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ci() {
        return Boolean(this.runningEnv.CI);
    }
    /**
     * See CI_PAGES_DOMAIN variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciPagesDomain() {
        return String(this.runningEnv.CI_PAGES_DOMAIN);
    }
    /**
     * See CI_PAGES_URL variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciPagesUrl() {
        return String(this.runningEnv.CI_PAGES_URL);
    }
    /**
     * See CI_PROJECT_ROOT_NAMESPACE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectRootNameSpace() {
        return String(this.runningEnv.CI_PROJECT_ROOT_NAMESPACE);
    }
    /**
     * See CI_PROJECT_NAMESPACE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectNameSpace() {
        return String(this.runningEnv.CI_PROJECT_NAMESPACE);
    }
    /**
     * See CI_PROJECT_NAME variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectName() {
        return String(this.runningEnv.CI_PROJECT_NAME);
    }
    /**
     * See CI_PROJECT_PATH variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectPath() {
        return String(this.runningEnv.CI_PROJECT_PATH);
    }
    /**
     * See CI_PROJECT_TITLE variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectTitle() {
        return String(this.runningEnv.CI_PROJECT_TITLE);
    }
    /**
     * See CI_PROJECT_URL variable in https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
     */
    get ciProjectUrl() {
        return String(this.runningEnv.CI_PROJECT_URL);
    }
}
exports.GitLabConfig = GitLabConfig;
