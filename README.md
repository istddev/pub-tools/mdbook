# MdBook

Boek Publicaties CLI voor Explainers en overige documentatie. Ter introductie zal:

- Eerst zal worden ingegaan op de MdBook CLI waarmee welke lokaal als in GitLab CI kan worden gebruikt

- Daarna de basis waarop een GitLab Pages publicatie mee kan worden gerealiseerd

## MdBook CLI

:CLI: waarmee **Markdown** content via een template op basis van het **VuePress Book Theme** kan worden gepubliceerd.

### Uitgangspunten content

De content als basis van een publicatie heeft een simpele opbouw op basis van ***Markdown**-bestanden en aanvullende media waarbij:

- Een ```SUMMARY.md``` bestand zoals deze in _GitBook_ wordt toegepast. *NB: De Titel(Header) zal hier worden gebruikt als "description" (beschrijvingsveld).* Zie voorbeeld [SUMMARY.md](https://gitlab.com/istddev/pub-tools/mdbook/-/blob/master/SUMMARY.md) uit de repository van deze publicatie.

- Daarnaast kan een ```logo.png``` bestand als _custom logo_ voor een publicatie aan repository worden toegevoegd.

### Gebruik CLI

Met de **MdBook** :cli: kan vanuit de simpele content een lokale website publicatie worden gegenereerd. Dit zowel op *handmatig*- als *geautomatiseerd* wijze via scripts.

#### Handmatig via Terminal

Direct vanuit de terminal kan door de volgende commando's achtereenvolgens uit te voeren een lokale website worden gepubliceerd. Doel is om daarmee direct her resultaat te beoordelen tijdens het wijzigen en/of uitbreiden daarvan.

1. Installeer **MdBook** package

```shell
npm install https://gitlab.com/istddev/pub-tools/mdbook.git
```

2. Parse de **Markdown**-bestanden en media op basis van de ```SUMMARY.md``` in de ```contentPath```-directory naar **VuePress**-formaat naar de ```docsPath```-directory

```shell
npx mdbook parse contentPath docsPath
```

3. Installeer **VuePress**-vereisten voor de **MdBook**-publicatie

```shell
npx mdbook vuepress
```

4. Maak de **MdBook**-publicatie op een lokale website beschikbaar^[Via het commando uit **stap 2** (```npx mdbook parse contentPath docsPath```) kunnen de laatste wijziging op de lokale website zichtbaar worden gemaakt.]. 

```shell
npx vuepress dev docsPath
```

#### Geautomatiseerd via NPM-package

De hiervoor beschreven handmatig uit te voeren commando's kunnen ook via onderstaande ```script``` in een ```package.json```-bestand worden geautomatiseerd:

```json
{
  "name": "mdbook-cli-example",
  "version": "0.0.1",
  "description": "Markdown Book Publicatie",
  "scripts": {
    "start": "npm run parse && mdbook vuepress && npm vuepress dev docsPath",
    "parse": "mdbook parse contentPath docsPath"
  },
  "devDependencies": {
    "@pub-tools/mdbook": "gitlab:istddev/pub-tools/mdbook"
  }
}
```

Daarna vanaf de command line:

- ```npm install``` om **MdBook**-package te installeren

- ```npm start``` waarna de lokale website beschikbaar komt

- ```npm run parse``` gebruikt kan worden om wijzigingen op de lokale website zichtbaar te maken

## GitLab Pages

Om een :gitlab: pages publicatie te realiseren moeten de volgende stappen worden doorlopen:

1. Maak een nieuw GitLab project :repository: aan en plaats daar minimaal een ```SUMMARY.md``` inclusief een daaraan gekoppeld **Markdown**-bestand met de naam ```README.md```. Zie onderstaande (minimale) voorbeeld:

```md
# Summary Md Book

## Over dit document

* [Leeswijzer](README.md)
```

2. Maak een ```.gitlab-ci.yml``` bestand aan met daarin:

```yml
include:
  - project: 'istddev/pub-tools/mdbook'
    file: '/.publish-mdbook.yml'
```

3. Start het script door via het GitLab menu **CI/CD > Pipelines** te selecteren. Druk daarna op de knop **Run pipeline** (rechtsboven). Daarna op de volgende pagina nogmaals op de knop **Run pipeline** drukken. Wacht tot het script is uitgevoerd (Parse - Export - Pages).

4. Na succesvol uitvoeren van **stap 3** komt de publicatie op **Pages** beschikbaar. Selecteer in het GitLab menu **Settings > Pages**. Daarna kun je via link bij *Access Pages* de website bezoeken.
