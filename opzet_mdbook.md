# Opzet Md Book

Deze publicatievorm is opgezet vanuit **GitBook** met **Markdown**-content als voorbeeld. Basis voor de content is de **SUMMARY** waarin alle referenties naar de publiceren content staan.

```ditaa

                              +----------------------+
+------------+         +--=-->| Sibbar Menu Item 1   |
| SUMMARY    |         |      +----------------------+
+------------+         |
|            |         |      +----------------------+
| Item 1     |----=----+--=-->| Sidebar Menu Item 2  |
| Item 2     |         |      +----------------------+
|            |         |
| Item (n)   |         |      
+------------+         |      +----------------------+
                       +--=-->| Sidebar Menu Item (n)|
                              +----------------------+

```
