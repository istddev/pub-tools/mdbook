FROM node:10.22.0

# install pandoc/latex requirements
RUN apt-get update
RUN apt-get install -y texlive-xetex
RUN apt-get install -y pandoc
