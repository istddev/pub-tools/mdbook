#!/usr/bin/env node
"use strict"

/**
 * Node Package Modules
 */
import { argv, env } from 'process';

/**
 * Local Library Modules
 */
import { parseMd2Book, GitLabConfig, MdBookConfig, InstallVuePressTemplateDependencies } from '../lib'

//
// START CLI Script
//

if (argv[2] || !argv[6]) {
    // create configuration from environment and CLI-parameters
    const gitLab = new GitLabConfig(env);

    if (gitLab.ci) {
        console.log('='.repeat(40));
        console.log('CI variables available in GitLabConfig (TypeScript)');
        console.log('-'.repeat(40));
        console.log(`ciPagesDomain = ${gitLab.ciPagesDomain}`);
        console.log(`ciPagesUrl = ${gitLab.ciPagesUrl}`);
        console.log(`ciProjectRootNameSpace = ${gitLab.ciProjectRootNameSpace}`);
        console.log(`ciProjectNameSpace = ${gitLab.ciProjectNameSpace}`);
        console.log(`ciProjectName = ${gitLab.ciProjectName}`);
        console.log(`ciProjectPath = ${gitLab.ciProjectPath}`);
        console.log(`ciProjectTitle = ${gitLab.ciProjectTitle}`);
        console.log(`ciProjectUrl = ${gitLab.ciProjectUrl}`);
        console.log('='.repeat(40));
    }

    const mdContentPath = argv[3] ? argv[3] : '.';
    const docsPath = argv[4] ? argv[4] : 'docs';
    const templatePath = argv[5];
    const config = new MdBookConfig(gitLab, { mdContentPath, docsPath, templatePath });

    // execute CLI-command
    const cliCommand = argv[2];

    switch (cliCommand) {

        case 'parse': {
            parseMd2Book(config);
            break;
        }

        case 'vuepress': {
            InstallVuePressTemplateDependencies(config);
            break;
        }

        default: {
            console.log('First argument (CLI-command) must be <parse | vuepress >!');
        }

    }

} else {
    console.error('First two parms are required: <parse | build | dev> <mdContentPath> <docsPath> (<vuePressTemplatePath> is optional)');
}
