import { existsSync } from 'fs';
import { resolve, join } from 'path';

import { copySync } from 'cpx';

import { resolveTemplatePath } from '@pub-tools/mdlib-ts';
import { GitLabConfig } from './gitlab-config';

/**
 * Mardown Book Publication Configuration
 */
export class MdBookConfig {
    private readonly vuePressConfig = '.vuepress';
    private readonly vuePressPublic = join(this.vuePressConfig, 'public');
    private readonly vuePressStyles = join(this.vuePressConfig, 'styles');
    private readonly vuePressConfigTemplateJs = join(this.vuePressConfig, 'config.template.js');
    private readonly vuePressDependenciesJson = join(this.vuePressConfig, 'vuepress-dependencies.json');
    private readonly vuePressConfigJs = join(this.vuePressConfig, 'config.js');
    private readonly siteMdFile = 'site.md';
    private readonly customLogoFile = 'logo.png';
    private publication = {
        base: '/',
        dest: 'public',
        logo: '/book.png',
        pdfExport: 'https://istddev.gitlab.io/pub-tools/mdbook/mdbook.pdf',
        epubExport: 'https://istddev.gitlab.io/pub-tools/mdbook/mdbook.epub',
        gitRepo: 'https://gitlab.com/istddev/pub-tools/mdbook'
    }
    /**
     * Site Export Language Metadata
     */
    public readonly lang = 'nl-NL';

    /**
     * Constructs Markdown Book Configuration Class
     * @param gitLab GitLab CI Running environment (in case of GitLab CI runner execution)
     * @param cliParms Command Line parameters
     */
    constructor(
        private gitLab: GitLabConfig,
        private cliParms: { 
            mdContentPath: string;
            docsPath: string;
            templatePath?: string;
        }) {

        if (this.gitLab.ci) {
            this.initGitLabCi();
        }

        this.setCustomLogo();
    }

    /**
     * Initialize Running GitLab CI Configuration values
     */
    private initGitLabCi() {

        if (!this.gitLab.isUserOrGroupPage) {
            // Set Markdown Book publication base path
            const repoPaths = this.gitLab.ciProjectPath.split('/');
            const repoGroup = repoPaths.length > 1 ? '/' + repoPaths[repoPaths.length - 2] : '';
            const repoProject = '/' + repoPaths[repoPaths.length - 1] + '/';
            this.publication.base = repoGroup + repoProject;
        }

        this.publication.pdfExport = this.gitLab.ciPagesUrl + '/' + this.gitLab.ciProjectName + '.pdf';
        this.publication.epubExport = this.gitLab.ciPagesUrl + '/' + this.gitLab.ciProjectName + '.epub';
        this.publication.gitRepo = this.gitLab.ciProjectUrl;
    }

    /**
     * Set custom publication logo ("logo.png" in repo Markdown content directory)
     */
    private setCustomLogo() {
        // custom logo should be available in Markdown content directory
        const customLogoFilePath = join(this.mdContentPath, this.customLogoFile);

        if (existsSync(customLogoFilePath)) {
            // copy en set custom defined logo
            copySync(customLogoFilePath, this.docsPublicPath);
            this.publication.logo = '/' + this.customLogoFile;
        }

    }

    /**
     * Resolved Output directory path to parse VuePress formated data
     */
    get docsPath() {
        return resolve(this.cliParms.docsPath);
    }

    /**
     * Resolved VuePress Public output directory path (output-files directly accessible in site publication)
     */
    get docsPublicPath() {
        return join(this.docsPath, this.vuePressPublic);
    }

    /**
     * Resolved VuePress Styles output directory path
     */
     get docsStylesPath() {
        return join(this.docsPath, this.vuePressStyles);
    }

    /**
     * Resolved Input directory path that should contain Md Book formated Markdown data
     */
    get mdContentPath() {
        return resolve(this.cliParms.mdContentPath);
    }

    /**
     * Resolved Path to VuePress Config Output JS File
     */
     get docsConfigJsFilePath() {
        return join(this.docsPath, this.vuePressConfigJs);
    }

    /**
     * Resolved VuePress Template directory path (MdBook-package or customomized-repo)
     */
    get templatePath() {
        return resolveTemplatePath(__dirname, this.cliParms.templatePath);
    }

    /**
     * Resolved Public template directory path (template-files directly accessible in site publication)
     */
    get templatePublicPath() {
        return join(this.templatePath, this.vuePressPublic);
    }

    /**
     * Resolved Path to VuePress Styles template directory
     */
    get templateStylesPath() {
        return join(this.templatePath, this.vuePressStyles);
    }

    /**
     * Resolved Path to VuePress Config Template JS File
     */
    get templateConfigJsFilePath() {
        return join(this.templatePath, this.vuePressConfigTemplateJs);
    }

    /**
     * Resolved Path to VuePress Depedencies JSON File
     */
     get vuePressDependenciesJsonFilePath() {
        return join(this.templatePath, this.vuePressDependenciesJson);
    }

    /**
     * Resolved Path to VuePress Site Export Input File (to create PDF, ePub, ...etc. exports)
     */
    get siteMdFilePath() {
        return join(this.docsPath, this.siteMdFile);
    }

    /**
     * GitLab Pages Publication base url path
     */
    get publicationBase() {
        return this.publication.base;
    }

    /**
     * GitLab Pages Publication destination directory path
     */
    get publicationDest() {
        return this.publication.dest;
    }

    /**
     * GitLab Pages Publication logo path
     */
     get publicationLogo() {
        return this.publication.logo;
    }

    /**
     * GitLab Pages Publication PDF export URL
     */
    get publicationPdfExport() {
        return this.publication.pdfExport;
    }

    /**
     * GitLab Pages Publication ePub export URL
     */
    get publicationEpubExport() {
        return this.publication.epubExport;
    }

    /**
     * GitLab Pages Publication GIT repository URL
     */
    get publicationGitRepo() {
        return this.publication.gitRepo;
    }

}